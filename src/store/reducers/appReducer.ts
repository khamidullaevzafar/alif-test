import { AppAction, AppActionTypes } from "./../../types/app"
import { AppState } from "../../types/app"
import { getRandomColor } from "../../utils/getRandomColor"
import moment from "moment"

const initialState: AppState = {
  timeString: moment().format("hh:mm:ss"),
  hallBackground: "#FFFFFF",
}

export const appReducer = (state = initialState, action: AppAction): AppState => {
  switch (action.type) {
    case AppActionTypes.SET_TIME_STRING:
      return { ...state, timeString: action.payload }

    case AppActionTypes.UPDATE_HALL_BACKGROUND:
      return { ...state, hallBackground: getRandomColor() }

    default:
      return state
  }
}
