import { AppAction, AppActionTypes } from "./../../types/app"
import { Dispatch } from "redux"
import moment from "moment"

let interval: NodeJS.Timer

export const beginTime = () => {
  return (dispatch: Dispatch<AppAction>) => {
    interval = setInterval(() => {
      const currentTime = new Date()
      const currentSecond = currentTime.getSeconds()
      dispatch({
        type: AppActionTypes.SET_TIME_STRING,
        payload: moment(currentTime).format("hh:mm:ss"),
      })
      if (currentSecond % 4 === 0)
        dispatch({ type: AppActionTypes.UPDATE_HALL_BACKGROUND })
    }, 1000)
  }
}

export const stopTime = () => {
  clearInterval(interval)
}
