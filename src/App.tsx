import "./App.scss"
import Hall from "./components/Hall"
import Home from "./components/Home"

function App() {
  return (
    <div className="App">
      <Home />
      <Hall />
    </div>
  )
}

export default App
