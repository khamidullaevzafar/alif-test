export enum AppActionTypes {
  SET_TIME_STRING = "SET_TIME_STRING",
  UPDATE_HALL_BACKGROUND = "UPDATE_HALL_BACKGROUND",
}

export interface AppState {
  timeString: string
  hallBackground: string
}

interface SetTimeStringAction {
  type: AppActionTypes.SET_TIME_STRING
  payload: string
}

interface UpdateHallBackground {
  type: AppActionTypes.UPDATE_HALL_BACKGROUND
}

export type AppAction = SetTimeStringAction | UpdateHallBackground
