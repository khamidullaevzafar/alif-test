import { FC } from "react"
import { useTypedSelector } from "../hooks/useTypedSelector"

const Hall: FC = () => {
  const background = useTypedSelector((state) => state.app.hallBackground)

  return (
    <div className="Hall" style={{ background }}>
      {background}
    </div>
  )
}

export default Hall
