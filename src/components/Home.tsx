import { FC } from "react"
import { useEffect } from "react"
import { useDispatch } from "react-redux"
import { useTypedSelector } from "../hooks/useTypedSelector"
import { beginTime, stopTime } from "../store/action-creators/appActionCreator"

const Home: FC = () => {
  const timeString = useTypedSelector((state) => state.app.timeString)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(beginTime())

    return () => {
      stopTime()
    }
  }, [])

  return (
    <div className="Home">
      <div className="time">{timeString}</div>
    </div>
  )
}

export default Home
