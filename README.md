# Test task for Alif Tech

## Available Scripts

In the project directory, you can run:

### Install dependencies
```
yarn install
```

### Runs the app in the development mode.
```
yarn start
```

### Builds the app for production
```
yarn build
```

![Alt text](preview.png)